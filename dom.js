var form = document.getElementById('addForm');
var itemlist = document.getElementById('items');
var filter = document.getElementById('filter');


form.addEventListener('submit',addItem);
// remove item
itemlist.addEventListener('click',removeItem);
// filter event
filter.addEventListener('keyup', filterItems);

// function for add items
function addItem(e) {
    e.preventDefault();
    // get input
var newItem = document.getElementById('item').value;

// create li
var li = document.createElement('li');
// add class to li
li.className = 'list-group-item';

// add text node with input value

li.appendChild(document.createTextNode(newItem));
// create button
var deleteBTN = document.createElement('button');

// alla classes to the delete buttton
deleteBTN.className = 'btn btn-danger btn-sm float-right delete';
// append textNode
deleteBTN.appendChild(document.createTextNode('X'));
// append button to li
li.appendChild(deleteBTN);
itemlist.appendChild(li);
    console.log(li);
    
}


// remove item
function removeItem(e) {
   if(e.target.classList.contains('delete'));{
    if(confirm('Are your sure?')){
        var li = e.target.parentElement;
        itemlist.removeChild(li);
    }
   }
    
}

// filter function
function filterItems(e) {
    //convert text to lower case
   var text = e.target.value.toLowerCase();

   
   // get list
   var items = itemlist.getElementsByTagName('li');
   // convert to an array
   Array.from(items).forEach(function(item) {
       
  var itemName = item.firstChild.textContent;
  if(itemName.toLowerCase().indexOf(text) != -1){
      item.style.display = 'block';
  }else{
      item.style.display = 'none';
  }
       
   });
}